#include <nds.h>
#include <stdlib.h>
#define NUM_STARS 40
typedef struct
{
  int x;
  int y;
  int speed;
  int dirx; //direction in x
  int diry; 
  unsigned short color;
}
Star;
Star stars[NUM_STARS];

//MoveStar Original
/*
void MoveStar(Star* star)
{
  star->x += star->speed;
  if (star->x >= SCREEN_WIDTH)
  {
    star->color = RGB15(31, 31, 31);
    star->x = 0;
    star->y = rand() % 192;
    star->speed = rand() % 4 + 1;
  }
}
*/

//Movestar nuevo
void MoveStar(Star* star)
{
  star->x += star->dirx*star->speed;
  star->y += star->diry*star->speed;
  if (star->x >= SCREEN_WIDTH || star->x <= 0 || star->y >= SCREEN_HEIGHT || star->y <=0)
  {
    star->color = RGB15(31, 31, 31);
    star->x = 128;
    star->y = 96;
    star->speed = rand() % 2 + 1;
    star->dirx = rand() % 10 + 1;
    star->diry = rand() % 10 + 1;
    if(star->dirx >=5)
    {
    star->dirx = -(rand() % 6);
    }
    if(star->diry >=5)
    {
      star->diry = -(rand() % 6);
    }
    //Si x e y son 0 damos un valor arbitrario para que no se quede la estrella quieta
    if(star->diry == 0 && star->dirx == 0)
    {
      star->dirx = 5;
    }
  }
}

void ClearScreen(void)
{
  int i;
  for (i = 0; i < 256 * 192; i++)
    VRAM_A[i] = RGB15(0, 0, 0);
}
/*
void InitStars(void)
{
  int i;
  for (i = 0; i < NUM_STARS; i++)
  {
    stars[i].color = RGB15(31, 31, 31);
    stars[i].x = rand() % 256;
    stars[i].y = rand() % 192;
    stars[i].speed = rand() % 4 + 1;
  }
}
*/
void InitStars(void)
{
  int i;
  for (i = 0; i < NUM_STARS; i++)
  {
    stars[i].color = RGB15(31, 31, 31);
    stars[i].x = 128;
    stars[i].y = 96;
    stars[i].speed = rand() % 2 + 1;
    stars[i].dirx = rand() % 10 + 1;
    stars[i].diry = rand() % 10 + 1;
    if(stars[i].dirx >=5)
    {
    stars[i].dirx = -(rand() % 5 + 1);
    }
    if(stars[i].diry >=5)
    {
      stars[i].diry = -(rand() % 5 + 1);
    }
  }
}
void DrawStar(Star* star)
{
  VRAM_A[star->x + star->y * SCREEN_WIDTH] = star->color;
}
void EraseStar(Star* star)
{
  VRAM_A[star->x + star->y * SCREEN_WIDTH] = RGB15(0, 0, 0);
}
int main(void)
{
  int i;
  irqInit();
  irqEnable(IRQ_VBLANK);
  videoSetMode(MODE_FB0);
  vramSetBankA(VRAM_A_LCD);
  ClearScreen();
  InitStars();
  //we like infinite loops in console dev!
  while (1)
  {
    swiWaitForVBlank();
    for (i = 0; i < NUM_STARS; i++)
    {
      EraseStar(&stars[i]);
      MoveStar(&stars[i]);
      DrawStar(&stars[i]);
    }
  }
  return 0;
}
