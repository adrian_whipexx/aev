/*---------------------------------------------------------------------------------

	Basic template code for starting a DS app

---------------------------------------------------------------------------------*/
#include <nds.h>
#include <stdio.h>
#include <system.h>
//---------------------------------------------------------------------------------
int main(void) {
//---------------------------------------------------------------------------------
	consoleDemoInit();
    /* Initialization done. Print welcome message. */
    iprintf("Practica 2!\n");

    //Nombre
    char name[11] = {0};
    int i;
    for(i=0; i<PersonalData->nameLen; i++){
    name[i] = (char)(PersonalData->name[i] & 255); // get ascii-bits from utf-16 name
    iprintf("%c",name[i]);
    }
    iprintf("\n");


    //Alarma
    int alarmah;
    alarmah = (int)(PersonalData->alarmHour & 255); // get ascii-bits from utf-16 name
    int alarmam;
    alarmam = (int)(PersonalData->alarmMinute & 255); // get ascii-bits from utf-16 name
    iprintf("%d:%d\n", alarmah, alarmam);


	while(1) {
		swiWaitForVBlank();
	}

}
