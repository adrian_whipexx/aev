#include <nds.h>

#include <stdio.h>
#include <stdlib.h>
#include <system.h>


void Nombre(void)
{
    char name[11] = {0};
    int i;
    for(i=0; i<PersonalData->nameLen; i++){
    name[i] = (char)(PersonalData->name[i] & 255); // get ascii-bits from utf-16 name
    iprintf("%c",name[i]);
    }
    iprintf("\n");
}