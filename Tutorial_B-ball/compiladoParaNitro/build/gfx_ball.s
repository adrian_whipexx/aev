
@{{BLOCK(gfx_ball)

@=======================================================================
@
@	gfx_ball, 16x16@4, 
@	+ palette 16 entries, not compressed
@	+ 4 tiles not compressed
@	Total size: 32 + 128 = 160
@
@	Time-stamp: 2015-02-10, 10:18:52
@	Exported by Cearn's GBA Image Transmogrifier, v0.8.12
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global gfx_ballTiles		@ 128 unsigned chars
	.hidden gfx_ballTiles
gfx_ballTiles:
	.word 0x11100000,0x11111000,0x1F11F100,0x11FFF110,0x1F1F1F10,0x33731111,0x33377B5C,0x23E2E75C
	.word 0x00000111,0x00011111,0x0011F11F,0x01111FF1,0x0111F11F,0x1111487B,0x1D7577AB,0x1D77E2EB
	.word 0x2BBBBA5C,0xAAA99852,0x2288847D,0x24444620,0x555562D0,0x777C2D00,0xDDDD1000,0x11100000
	.word 0x1DC65482,0x1D299448,0x112C6522,0x01D2C652,0x011D2C76,0x0011D22C,0x000111DD,0x00000111

	.section .rodata
	.align	2
	.global gfx_ballPal		@ 32 unsigned chars
	.hidden gfx_ballPal
gfx_ballPal:
	.hword 0x001F,0x0C83,0x1505,0x77FD,0x26AA,0x2229,0x1DE8,0x19A7
	.hword 0x372E,0x5599,0x4B73,0x5FB7,0x1946,0x10C4,0x7FFF,0x0897

@}}BLOCK(gfx_ball)
