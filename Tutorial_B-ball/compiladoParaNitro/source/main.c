
/* INCLUDES */

#include <nds.h>

//-----------------------------------------------
// graphic references
//-----------------------------------------------
#include "gfx_ball.h"
#include "gfx_brick.h"
#include "gfx_gradient.h"


/* DEFINES */
//-----------------------------------------------
// tile entries
//-----------------------------------------------

#define tile_empty     0 // tile 0 = empty
#define tile_brick     1 // tile 1 = brick
#define tile_gradient  2 // tile 2 to 9 = gradient

// macro for calculating BG VRAM memory
// address with tile index
#define tile2bgram(t) (BG_GFX + (t) * 16)

// BG PALETTES
#define pal_bricks        0    // brick palette (entry 0->15)
#define pal_gradient      1    // gradient palette (entry 16->31)

#define backdrop_colour   RGB8( 190, 225, 255 )
#define pal2bgram(p)      (BG_PALETTE + (p) * 16)

// TILEMAP
#define bg0map    ((u16*)BG_MAP_RAM(1))
#define bg1map    ((u16*)BG_MAP_RAM(2))

// SPRITES
#define tiles_ball      0 // ball tiles (16x16 tile 0->3)
#define tile2objram(t) (SPRITE_GFX + (t) * 16)

// PALETTE FOR BALL
#define pal2objram(p) (SPRITE_PALETTE + (p) * 16)
#define pal_ball      0 // ball palette (entry 0->15)

//BALL
#include "ball.h"
ball g_ball;
#define c_diam              16                 // the diameter of the ball (integer)

typedef struct t_spriteEntry
{
    u16 attr0;
    u16 attr1;
    u16 attr2;
    u16 affine_data;
} spriteEntry;

#define sprites ((spriteEntry*)OAM)

// LOGIC 
#define x_tweak    (2<<8)  // for user input
#define y_tweak    25      // for user input

//JUMP FUNCTIONALITY
bool isPressed =   0;      //is 1 if UP is pressed

//CAMERA
int g_camera_x;
int g_camera_y;

/* CODE */

//-----------------------------------------------------------
// setup interrupt handler with vblank irq enabled
//-----------------------------------------------------------
void setupInterrupts( void )
//-----------------------------------------------------------
{
    // initialize interrupt handler
    irqInit();
    
    // enable vblank interrupt (required for swiWaitForVBlank!)
    irqEnable( IRQ_VBLANK );
}

//-----------------------------------------------------------
// update graphical information (call during vblank)
//-----------------------------------------------------------
void updateGraphics( void )
//-----------------------------------------------------------
{
    // update ball sprite
    ballRender( &g_ball, g_camera_x >> 8, g_camera_y >> 8 );
    
    REG_BG0HOFS = g_camera_x >> 8;
}

//-----------------------------------------------------------
// reset ball attributes
//-----------------------------------------------------------
void resetBall( void )
//-----------------------------------------------------------
{
    // use sprite index 0 (0->127)
    g_ball.sprite_index = 0;
    
    // use affine matrix 0 (0->31)
    g_ball.sprite_affine_index = 0;
    
    // X = 128.0
    g_ball.x = 128 << 8;
    
    // Y = 64.0
    g_ball.y = 64 << 8;
   
    // start X velocity a bit to the right
    g_ball.xvel = 100 << 4;
   
    // reset Y velocity
    g_ball.yvel = 0;
}

void processLogic( void )
{
    processInput();
    ballUpdate( &g_ball );
    updateCamera();
}

void setupGraphics( void )
{
    vramSetBankE( VRAM_E_MAIN_BG );
    vramSetBankF( VRAM_F_MAIN_SPRITE );

    // generate the first blank tile by clearing it to zero
    int n;
    for( n = 0; n < 16; n++ )
        BG_GFX[n] = 0;
    
    // copy bg graphics
    dmaCopyHalfWords( 3, gfx_brickTiles, tile2bgram( tile_brick ), gfx_brickTilesLen );
    dmaCopyHalfWords( 3, gfx_gradientTiles, tile2bgram( tile_gradient ), gfx_gradientTilesLen );

    // palettes goto palette memory
    dmaCopyHalfWords( 3, gfx_brickPal, pal2bgram(pal_bricks), gfx_brickPalLen );
    dmaCopyHalfWords( 3, gfx_gradientPal, pal2bgram(pal_gradient), gfx_gradientPalLen );

    // set backdrop color
    BG_PALETTE[0] = backdrop_colour;

    /* BRICKS */

    // libnds prefixes the register names with REG_
    REG_BG0CNT = BG_MAP_BASE(1);
    REG_BG1CNT = BG_MAP_BASE(2);

    //int n;
    for( n = 0; n < 1024; n++ )
        bg0map[n] = 0;
    //offset to place bricks on the bottom of the screen
    REG_BG0VOFS = 112;

    //draw bricks
    int x, y;
    for( x = 0; x < 32; x++ )
    {
        for( y = 0; y < 6; y++ )
        {
            // magical formula to calculate if the tile needs to be flipped.
            int hflip = (x & 1) ^ (y & 1);
            
            // set the tilemap entry
            bg0map[x + y * 32] = tile_brick | (hflip << 10) | (pal_bricks << 12);
        }
    }

    /* GRADIENT */
    //Blending BG1 against the backdrop
    REG_BLDCNT = BLEND_ALPHA | BLEND_SRC_BG1 | BLEND_DST_BACKDROP;
    REG_BLDALPHA = (4) + (16<<8);

    //we clear bg1
     for( n = 0; n < 1024; n++ )
        bg1map[n] = 0;   

    //we fill the upper 256x64 region (32x8 tiles) with the gradient tiles
    //int x, y;
    for( x = 0; x < 32; x++ )
    {
        for( y = 0; y < 8; y++ )
        {
            int tile = tile_gradient + y;
            bg1map[ x + y * 32 ] = tile | (pal_gradient << 12);
        }
    }

    /* SRPITES */
    //copy the tiles into VRAM
    dmaCopyHalfWords( 3, gfx_ballTiles, tile2objram(tiles_ball), gfx_ballTilesLen );
    dmaCopyHalfWords( 3, gfx_ballPal, pal2objram(pal_ball), gfx_ballPalLen );

    //Seting entries for sprites to disabled beffore setting the video mode
    for( n = 0; n < 128; n++ )
        sprites[n].attr0 = ATTR0_DISABLED;

    videoSetMode( MODE_0_2D | DISPLAY_BG0_ACTIVE | DISPLAY_BG1_ACTIVE | DISPLAY_SPR_ACTIVE | DISPLAY_SPR_1D_LAYOUT );


}

void processInput( void )
{
    scanKeys();
    int keysh = keysHeld();
    // process user input
    if( keysh & KEY_UP )      // check if UP is pressed
    {
        if ( !(isPressed) )
        {
            isPressed = 1;
            g_ball.yvel = -(30*y_tweak);     
        }
        // tweak y velocity of ball
        g_ball.yvel -= y_tweak;
    }
    else{
        if (g_ball.height < c_diam << 8  )
        {
            isPressed = 0;            
        }
    }
    if( keysh & KEY_DOWN )    // check if DOWN is pressed
    {
        // tweak y velocity of ball
        g_ball.yvel += y_tweak;
    }
    if( keysh & KEY_LEFT )    // check if LEFT is pressed
    {
        // tweak x velocity
        g_ball.xvel -= x_tweak;
    }
    if( keysh & KEY_RIGHT )   // check if RIGHT is pressed
    {
        // tweak y velocity
        g_ball.xvel += x_tweak;
    }
    
}

void updateCamera( void )
{
    // cx = desired camera X
    int cx = ((g_ball.x)) - (128 << 8);
    
    // dx = difference between desired and current position
    int dx;
    dx = cx - g_camera_x;
    
    // 10 is the minimum threshold
    if( dx > 10 || dx < -10 )
        dx = (dx * 50) >> 10; // scale the value by some amount
    
    // add the value to the camera X position
    g_camera_x += dx;
    
    // camera Y is always 0
    g_camera_y  = 0;
}


/* MAIN */

//-----------------------------------------------------------
// program entry point
//-----------------------------------------------------------
int main( void )
//-----------------------------------------------------------
{
    // setup things
    setupInterrupts();
    setupGraphics();
    resetBall();

    // start main loop
    while( 1 )
    {
        // update game logic
        processLogic();

        // wait for new frame
        swiWaitForVBlank();

        // update graphics
        updateGraphics();
    }
    return 0;
}
